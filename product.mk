TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := aarch64

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/aarch64-linux-musl-cross/bin/aarch64-linux-musl-
#TARGET_UBOOT_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-gnueabihf/bin/arm-linux-gnueabihf-
#TARGET_LINUX_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-gnueabihf/bin/arm-linux-gnueabihf-

#TARGET_GLOBAL_CFLAGS += -march=armv8-a

#TARGET_LINUX_IMAGE := uImage

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TOP_DIR)/trail/rpi3/config/

TARGET_UBI_IMAGE_OPTIONS := \
       --mkubifs="-m 1 -e 65408 -c 247 -x favor_lzo -F" \
       --ubinize="-p 0x10000 -m 1 $(TARGET_CONFIG_DIR)/ubinize.config"

LXC_KEEP := \
	usr/bin/lxc-console \
	usr/bin/lxc-info \
	usr/bin/lxc-ls \
	usr/bin/lxc-top \
	$(NULL)

LXC_TRIM := \
	usr/bin/lxc-attach \
	usr/bin/lxc-autostart \
	usr/bin/lxc-cgroup \
	usr/bin/lxc-checkconfig \
	usr/bin/lxc-checkpoint \
	usr/bin/lxc-config \
	usr/bin/lxc-copy \
	usr/bin/lxc-create \
	usr/bin/lxc-destroy \
	usr/bin/lxc-device \
	usr/bin/lxc-execute \
	usr/bin/lxc-freeze \
	usr/bin/lxc-monitor \
	usr/bin/lxc-snapshot \
	usr/bin/lxc-start \
	usr/bin/lxc-stop \
	usr/bin/lxc-unfreeze \
	usr/bin/lxc-unshare \
	usr/bin/lxc-update-config \
	usr/bin/lxc-usernsexec \
	usr/bin/lxc-wait  \
	$(NULL)

E2FS_TRIM := \
	usr/sbin/mkfs.ext3 \
	usr/sbin/mkfs.ext3 \
	usr/sbin/mkfs.ext2 \
	usr/sbin/mke2fs \
	usr/sbin/resize2fs \
	$(NULL)

LVM2_TRIM := \
	etc/lvm/*/* \
	etc/lvm/* \
	usr/sbin/lv* \
	usr/sbin/vg* \
	usr/sbin/pv* \
	usr/sbin/dmstats \
	$(NULL)

OPENSSL_TRIM := \
	usr/ssl/*/* \
	usr/ssl/* \
	usr/bin/openssl \
	$(NULL)

DROPBEAR_TRIM := \
	usr/bin/dbclient \
	usr/bin/dropbear* \
	$(NULL)

UBOOT_TRIM := \
	boot/u-boot.bin \
	$(NULL)



TARGET_FINAL_TRIM := \
	$(LXC_TRIM) \
	$(LVM2_TRIM) \
	$(OPENSSL_TRIM) \
	$(DROPBEAR_TRIM) \
	$(NULL)

LINUX_CONFIG_TARGET := bcm2711_defconfig
TARGET_LINUX_CONFIG_MERGE_FILES := yes

INITRD_CONFIG := trail.config
